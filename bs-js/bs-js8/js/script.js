const inPut = document.createElement('input')
inPut.style.display = 'block'
const sPan = document.createElement('span')
const btn = document.createElement('button')

btn.innerHTML = 'X'
inPut.placeholder = 'price'


inPut.addEventListener('focus', () => inPut.className = 'input-border')


inPut.addEventListener('blur', function() {
if(inPut.value < 0 || parseInt(inPut.value)){
    inPut.before(sPan)
    sPan.after(btn)
    sPan.innerHTML = `Текущая цена: ${inPut.value}`
    sPan.style.display = 'inline-block'
    btn.style.display = 'inline-block'
    inPut.classList.remove('input-border')
    inPut.style.color = 'green'
}else{
    inPut.after(sPan)
    sPan.innerHTML = 'Please enter correct price'
    sPan.style.display = 'inline-block'
    sPan.style.color = 'red'

}
})
btn.addEventListener('click', function(){
    sPan.style.display = 'none'
    btn.style.display = 'none'
    inPut.style.color = 'black'
    inPut.value = ''

})

document.body.after(inPut)
console.log(inPut.focus)  
