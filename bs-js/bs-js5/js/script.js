(function createNewUser() {
    let firstName = prompt('write your first name');
    let lastName = prompt('write your last name');
    let birthday = prompt('your birthday, dd.mm.yyyy')

    const newUser = {
        // firstName: 'firstName',
        // lastName,

        getAge() {

            let today = new Date();
            let dayToday = today.getDate();
            let monthToday = today.getMonth() + 1;
            let yearToday = today.getFullYear();

            if (dayToday < 10) {
                dayToday = '0' + dayToday;
            }
            if (monthToday < 10) {
                monthToday = '0' + monthToday;
            }
            const theDayUser = birthday.slice(0, 2) + '.';
            const theMonthUser = birthday.slice(3, 5) + '.';
            const theYearUser = birthday.slice(6, 10);

            let age = yearToday - theYearUser;
            let monthDiffer = monthToday - theMonthUser;
            if (monthDiffer < 0 || (monthDiffer === 0 && dayToday < theDayUser)) {
                age--;
            }
            return age;
        },

        getLogin() {
            let psW = firstName.slice(0, 1) + lastName
            return psW.toLowerCase()
        },

        getPassword() {
            return firstName.toUpperCase()[0] + lastName.toLowerCase() + birthday.slice(6, 10)
        }

    }

    Object.defineProperty(newUser, 'firstName', {
        get: function() { return firstName; },
        set: function(fName) { firstName = fName; },
        enumerable: true,
        configurable: true
    })
    Object.defineProperty(newUser, 'lastName', {
        get: function() { return lastName; },
        set: function(lName) { lastName = lName; },
        enumerable: true,
        configurable: true
    })
    Object.defineProperty(newUser, 'birthday', {
        get: function() { return birthday; },
        set: function(bDay) { birthday = bDay; },
        enumerable: true,
        configurable: true

    })

    console.log(newUser)
    console.log(newUser.getLogin())
    console.log(newUser.getAge())
    console.log(newUser.getPassword())


})()