const array = ['hello', 'world', 23, '23', null]
const string = 'string'

const filterBy = (arr, typ) => arr.filter(el => typeof el !== typeof typ)

console.log(filterBy(array, string))