let nameUser = prompt('your name')
let lastNameUser = prompt('your last name')
const student = {
    name: nameUser,
    lastName: lastNameUser,
}

// const tabel = {}
const tabObj = function() {
    let userSubj
    let userGrade
    const tabel = {}

    while (userSubj == undefined || userSubj || userGrade) {
        userSubj = prompt('your subject')
        userGrade = +prompt('your grade')
        if (!userSubj || !userGrade) {
            break
        } else {
            tabel[userSubj] = userGrade
        }
    }
    return tabel
}

student.tabel = tabObj()

function sumGrades(grades) {
    let sum = 0
    let counter = 0
    let result = 0

    for (let key of Object.values(grades)) {
        sum += key
        counter++
    }
    result = sum / counter
    if (result > 7) {
        alert('Студенту назначена стипендия')
    }

    let minGrades = Math.min.apply(null, Object.values(grades))

    if (minGrades < 4) {
        console.log(minGrades)
    } else {
        alert('Студент переведен на следующий курс')
    }
}

sumGrades(student.tabel)
console.log(student)