const bbt = document.getElementById('bbt')
const inPut = document.createElement('input')
inPut.className = 'in-put'
inPut.placeholder = 'диаметр круга'
const drawBtn = document.createElement('button')
drawBtn.innerText = 'Нарисовать'
drawBtn.id = 'bbt'

bbt.addEventListener('click', () => {
    document.body.after(inPut)
    inPut.after(drawBtn)
})

function getRandom(min, max) {
    return Math.ceil(Math.random() * (max - min) + min)
}
const drawCircle = () => {
    let num = inPut.value

    for (let index = 0; index <= 99; index++) {

        const cirCle = document.createElement('div')
        cirCle.className = 'circle'
        cirCle.style.width = num + 'px'
        cirCle.style.height = num + 'px'
        drawBtn.after(cirCle)
        cirCle.style.backgroundColor = `rgb(${getRandom(0, 255)}, ${getRandom(0, 255)}, ${getRandom(0, 255)})`;

    }
    const cirCles = document.querySelectorAll('div.circle')
    cirCles.forEach(el => {
        el.addEventListener('click', () => {
            el.style.display = 'none'


        })
    })
}



drawBtn.addEventListener('click', drawCircle)